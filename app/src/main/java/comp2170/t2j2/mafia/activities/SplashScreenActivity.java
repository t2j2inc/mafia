package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import comp2170.t2j2.mafia.R;

public class SplashScreenActivity extends Activity {

    private ImageView mafiaLogo;
    private Intent mainMenuIntent;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        mafiaLogo = (ImageView) findViewById(R.id.mafiaLogo);
        mainMenuIntent = new Intent(this, MainMenuActivity.class);
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(mainMenuIntent);
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
