package comp2170.t2j2.mafia.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import comp2170.t2j2.mafia.R;


public class MainMenuActivity extends Activity implements View.OnClickListener {

    private Intent intent;
    private ImageButton startGame;
    private ImageButton options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu);
        if(Build.VERSION.SDK_INT >= 11) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        startGame = (ImageButton) findViewById(R.id.startGame);
        startGame.setOnClickListener(this);
        options = (ImageButton) findViewById(R.id.options);
        options.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.startGame:
                intent = new Intent(this, PlayersActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.options:
                intent = new Intent(this, OptionsActivity.class);
                startActivity(intent);
                break;
        }

    }
}
