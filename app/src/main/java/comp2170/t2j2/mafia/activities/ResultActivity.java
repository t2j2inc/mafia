package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import comp2170.t2j2.mafia.R;

public class ResultActivity extends Activity implements View.OnClickListener {

    private TextView gameWinner;
    private Button continuepast;
    private Intent intent;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);
        gameWinner = (TextView)findViewById(R.id.winner);
        Bundle b = getIntent().getBundleExtra("GAME RESULT");
        String winner = b.getString("GAME WINNER");
        gameWinner.setText("The winner is: \n" + winner);
        continuepast = (Button) findViewById(R.id.cbutton);
        continuepast.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cbutton:
                intent = new Intent(this, MainMenuActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
