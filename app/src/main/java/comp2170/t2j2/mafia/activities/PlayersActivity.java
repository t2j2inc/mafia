package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import comp2170.t2j2.mafia.R;

import comp2170.t2j2.mafia.utils.PlayerCollection;

public class PlayersActivity extends Activity {
    public static final String SELECTED_PLAYERS_KEY = "SELECTED PLAYERS FOR NEW GAME";
    public static final String SELECTED_PLAYERS_BUNDLE_KEY = "BUNDLE OF SELECTED PLAYERS";
    private ArrayList<String> allPlayers;
    private PlayerCollection playerManager;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.players);
        playerManager = PlayerCollection.get(this);
        allPlayers = playerManager.getPlayerNames();
        final ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new ArrayAdapter<String>(this, R.layout.player_list_item , allPlayers);
        listView.setAdapter(adapter);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            registerForContextMenu(listView);
        else {
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                    //Maybe later
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.fragment_list_item_context, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()){
                        case R.id.menu_item_delete_player:
                            for(int i = adapter.getCount() - 1; i >= 0; i--){
                                if(listView.isItemChecked(i)){
                                    playerManager.deletePlayer(i);
                                    allPlayers.remove(i);

                                }
                            }
                            playerManager.savePlayers();
                            mode.finish();
                            adapter.notifyDataSetChanged();
                            return true;
                        case R.id.add_player_to_game:
                            String[] names = new String[8];
                            int j = 0;
                            for(int i = 0; i < adapter.getCount(); i++){
                                if(listView.isItemChecked(i)){
                                    names[j++] = (String)listView.getItemAtPosition(i);
                                }
                            }
                            if(j != 8)
                                Toast.makeText(getApplicationContext(), R.string.add_player_error, Toast.LENGTH_SHORT).show();
                            else{
                                Intent i = new Intent(getApplicationContext() , GameActivity.class);
                                Bundle b = new Bundle();
                                b.putStringArray(SELECTED_PLAYERS_KEY , names);
                                i.putExtra(SELECTED_PLAYERS_BUNDLE_KEY , b);
                                startActivity(i);
                                finish();
                            }
                        default:
                            return true;
                    }

                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {

                }
            });
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        //Inflate action bar menu...
        getMenuInflater().inflate(R.menu.fragment_player_list , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()){
            case R.id.menu_item_new_player:
                i = new Intent(this , CreatePlayerActivity.class);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.fragment_list_item_context ,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int position = info.position;
        Intent i;
        switch (item.getItemId()){
            case R.id.menu_item_delete_player:
                playerManager = PlayerCollection.get(this);
                playerManager.deletePlayer(position);
                playerManager.savePlayers();
                allPlayers.remove(position);
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
