package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import comp2170.t2j2.mafia.R;
import comp2170.t2j2.mafia.models.Player;
import comp2170.t2j2.mafia.utils.PlayerCollection;

public class CreatePlayerActivity extends Activity{

    private EditText name;
    private PlayerCollection players;
    private Button submit;
    private Intent intent;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createplayer);
        name = (EditText) findViewById(R.id.playerName);
        submit = (Button) findViewById(R.id.submit);
        final PlayerCollection collection = PlayerCollection.get(this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String playerName = name.getText().toString();
                Player player = new Player(playerName);
                collection.addPlayer(player);
                collection.savePlayers();
                for (Player p : collection.getPlayers()) {
                    Log.d("PLAYER", p.getName());
                }
                Toast.makeText(getBaseContext(), playerName + " has been created", Toast.LENGTH_SHORT).show();
                intent = new Intent(getApplicationContext(), PlayersActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }



}

