package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import comp2170.t2j2.mafia.R;

public class OptionsActivity extends Activity implements View.OnClickListener {

    private CheckBox toggle;
    private Button leave;
    private Intent intent;
    private MediaPlayer backgroundmusic;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options);
        toggle = (CheckBox) findViewById(R.id.togglemusic);
        leave = (Button) findViewById(R.id.leaveoptions);
        backgroundmusic = MediaPlayer.create(this, R.raw.backgroundmusic);
        toggle.setOnClickListener(this);
        leave.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){
         switch(v.getId()){
              case R.id.togglemusic:
                     if (!(backgroundmusic.isPlaying())){
                           backgroundmusic.start();
                     }
                     else{
                           backgroundmusic.stop();
                     }
                     break;
              case R.id.leaveoptions:
                      intent = new Intent(getBaseContext(), MainMenuActivity.class);
                      startActivity(intent);
                      break;
         }
    }
    protected void onPause(){
        super.onPause();
        finish();
    }
}
