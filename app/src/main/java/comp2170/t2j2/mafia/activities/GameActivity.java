package comp2170.t2j2.mafia.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import comp2170.t2j2.mafia.R;
import comp2170.t2j2.mafia.models.Game;
import comp2170.t2j2.mafia.models.Phase;
import comp2170.t2j2.mafia.models.Player;
import comp2170.t2j2.mafia.models.Role;


public class GameActivity extends Activity implements View.OnClickListener {

    private ImageButton forward;
    private ImageButton quit;
    private ImageButton player1;
    private ImageButton player2;
    private ImageButton player3;
    private ImageButton player4;
    private ImageButton player5;
    private ImageButton player6;
    private ImageButton player7;
    private ImageButton player8;
    private TextView roundNumberTextView;
    private TextView playerName1;
    private TextView playerName2;
    private TextView playerName3;
    private TextView playerName4;
    private TextView playerName5;
    private TextView playerName6;
    private TextView playerName7;
    private TextView playerName8;
    private ArrayList<TextView> voteCounters;
    private Player markedPlayer;
    private Player rescuePlayer;
    private Intent intent;
    private ArrayList<Player> players;
    private Game game;
    private LinearLayout background;
    private ArrayList<ImageButton> buttons;
    private MediaPlayer mp;
   // private BackgroundMusicService backgroundMusicService = new BackgroundMusicService();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        mp = MediaPlayer.create(this , R.raw.backgroundmusic);
        mp.setVolume(100, 100);
        mp.setLooping(true);
        mp.start();
        Bundle b = getIntent().getBundleExtra(PlayersActivity.SELECTED_PLAYERS_BUNDLE_KEY);
        setupPlayers(b);
        voteCounters = new ArrayList<>();
        buttons = new ArrayList<>();
        background = (LinearLayout)findViewById(R.id.game_background);
        forward = (ImageButton) findViewById(R.id.pauseGame);
        forward.setOnClickListener(this);
        quit = (ImageButton) findViewById(R.id.endGame);
        quit.setOnClickListener(this);
        /*Set Up players */

        player1 = (ImageButton) findViewById(R.id.p1);
        player1.setOnClickListener(this);
        setBackground(players.get(0) , player1);
        buttons.add(player1);
        playerName1 = (TextView)findViewById(R.id.p1name);
        playerName1.setText(players.get(0).getName());


        player2 = (ImageButton) findViewById(R.id.p2);
        player2.setOnClickListener(this);
        setBackground(players.get(1) , player2);
        playerName2 = (TextView)findViewById(R.id.p2name);
        playerName2.setText(players.get(1).getName());
        buttons.add(player2);

        player3 = (ImageButton) findViewById(R.id.p3);
        player3.setOnClickListener(this);
        setBackground(players.get(2) , player3);
        playerName3 = (TextView)findViewById(R.id.p3name);
        playerName3.setText(players.get(2).getName());
        buttons.add(player3);

        player4 = (ImageButton) findViewById(R.id.p4);
        player4.setOnClickListener(this);
        setBackground(players.get(3) , player4);
        buttons.add(player4);
        playerName4 = (TextView)findViewById(R.id.p4name);
        playerName4.setText(players.get(3).getName());

        player5 = (ImageButton) findViewById(R.id.p5);
        player5.setOnClickListener(this);
        setBackground(players.get(4) , player5);
        buttons.add(player5);
        playerName5 = (TextView)findViewById(R.id.p5name);
        playerName5.setText(players.get(4).getName());

        player6 = (ImageButton) findViewById(R.id.p6);
        player6.setOnClickListener(this);
        setBackground(players.get(5) , player6);
        buttons.add(player6);
        playerName6 = (TextView)findViewById(R.id.p6name);
        playerName6.setText(players.get(5).getName());

        player7 = (ImageButton) findViewById(R.id.p7);
        player7.setOnClickListener(this);
        setBackground(players.get(6) , player7);
        buttons.add(player7);
        playerName7 = (TextView)findViewById(R.id.p7name);
        playerName7.setText(players.get(6).getName());

        player8 = (ImageButton) findViewById(R.id.p8);
        player8.setOnClickListener(this);
        setBackground(players.get(7) , player8);
        buttons.add(player8);
        playerName8 = (TextView)findViewById(R.id.p8name);
        playerName8.setText(players.get(7).getName());

        roundNumberTextView = (TextView)findViewById(R.id.roundnum);
        roundNumberTextView.setText("Round: " + game.getRound());
        voteCounters.add((TextView)findViewById(R.id.p1votes));
        voteCounters.add((TextView)findViewById(R.id.p2votes));
        voteCounters.add((TextView)findViewById(R.id.p3votes));
        voteCounters.add((TextView)findViewById(R.id.p4votes));
        voteCounters.add((TextView)findViewById(R.id.p5votes));
        voteCounters.add((TextView)findViewById(R.id.p6votes));
        voteCounters.add((TextView)findViewById(R.id.p7votes));
        voteCounters.add((TextView)findViewById(R.id.p8votes));
    }

    @Override
    public void onClick(View v) {
        players = game.getPlayers();
        switch (v.getId()){
            case R.id.pauseGame:
               //Log.d("GAME TURN" , game.turn.toString());
                if(!game.isGameOver()) {
                    if (game.turn == Role.MAFIA) {
                        markedPlayer = game.mostVotesPlayer();
                        findPlayerVisits();
                        game.turn = Role.DOCTOR;
                        game.resetPlayerVotes();
                        zeroOutCounters();
                        //Log.d("GAME VOTE", markedPlayer.getName());
                        Toast.makeText(this, " Doctor's turn ", Toast.LENGTH_SHORT).show();
                    } else if (game.turn == Role.DOCTOR) {
                        rescuePlayer = game.mostVotesPlayer();
                        findPlayerVisits();
                        game.turn = Role.SHERIFF;
                        game.resetPlayerVotes();
                        zeroOutCounters();
                        if (markedPlayer != null && rescuePlayer != null) {
                            if (markedPlayer.equals(rescuePlayer))
                                Toast.makeText(this, " Doctor saved a player", Toast.LENGTH_SHORT).show();
                            else {
                                game.removePlayer(markedPlayer);
                                Toast.makeText(this, " The mafia killed " + markedPlayer.getName(), Toast.LENGTH_SHORT).show();
                                players = game.getPlayers();
                                for (int i = 0; i < players.size(); i++) {
                                    setBackground(players.get(i), buttons.get(i));
                                }
                            }
                        }
                        //Log.d("GAME VOTE" , rescuePlayer.getName());
                        Toast.makeText(this, " Sheriff's turn ", Toast.LENGTH_SHORT).show();
                    } else if (game.turn == Role.SHERIFF) {
                        Player investigatedPlayer = game.mostVotesPlayer();
                        findPlayerVisits();
                        if(investigatedPlayer != null) {
                            if (investigatedPlayer.getVisit() == null)
                                Toast.makeText(this, investigatedPlayer.getName() + " visited nobody ", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(this, investigatedPlayer.getName() + " visited " + investigatedPlayer.getVisit().getName(), Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(this , "Village Turn" , Toast.LENGTH_SHORT).show();
                        game.resetPlayerVotes();
                        zeroOutCounters();
                        //Log.d("GAME VOTE" , investigatedPlayer.getName());
                        game.turn = Role.VILLAGER;
                        game.changePhase();

                        setUpGameBackground();
                    } else {
                        Player lynchedPlayer = game.mostVotesPlayer();
                        game.changePhase();
                        if(lynchedPlayer != null) {
                            game.removePlayer(lynchedPlayer);
                            Toast.makeText(this, "The Village lynched " + lynchedPlayer.getName(), Toast.LENGTH_SHORT).show();
                        }
                        setUpGameBackground();
                        players = game.getPlayers();
                        for (int i = 0; i < players.size(); i++) {
                            setBackground(players.get(i), buttons.get(i));
                            //Log.d("CHANGE" ,players.get(i).getName() );
                        }
                        roundNumberTextView.setText("Round: " + game.getRound());
                        //Log.d("GAME VOTE" , lynchedPlayer.getName());
                        game.resetPlayerVotes();
                        zeroOutCounters();
                        game.turn = Role.MAFIA;
                        Toast.makeText(this, " Mafia turn ", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(this , .getName() + " was killed.", Toast.LENGTH_SHORT).show();
                    }
                }
            else{
                Intent i = new Intent(this , ResultActivity.class);
                Bundle b = new Bundle();
                b.putString("GAME WINNER" , game.getWinner().toString());
                i.putExtra("GAME RESULT" , b);
                startActivity(i);
                finish();
            }
                break;

            case R.id.endGame:
                intent = new Intent(getBaseContext(), MainMenuActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                for(int i = 0; i < 8; i++) {
                    if (v.getId() == buttons.get(i).getId()) {
                        players.get(i).incrementVotes();
                        voteCounters.get(i).setText("Votes "+ players.get(i).getVotes());
                    }
                }

        }
    }

    private void findPlayerVisits() {
        for(Player p : players){
            if(p.getRole().equals(game.turn)){
                p.castVote(game.mostVotesPlayer());
            }
        }
    }

    public void setupPlayers(Bundle b){
        this.players = new ArrayList<>();
        for(String s: b.getStringArray(PlayersActivity.SELECTED_PLAYERS_KEY)){
            players.add(new Player(s));
        }
        game = new Game(this.players);
        this.players = game.getPlayers();
    }

    public void setBackground(Player player , ImageButton button){
        switch (player.getRole()){
            case DOCTOR:
                button.setBackgroundResource(R.drawable.doctor);
                break;
            case VILLAGER:
                button.setBackgroundResource(R.drawable.villager);
                break;
            case MAFIA:
                button.setBackgroundResource(R.drawable.mafia);
                break;
            case SHERIFF:
                button.setBackgroundResource(R.drawable.sheriff);
                break;
            case DEAD:
                button.setBackgroundResource(R.drawable.dead_player);
                button.setEnabled(false);
                break;
        }


    }

    public void setUpGameBackground(){
        if(game.getPhase() == Phase.DAY){
            background.setBackgroundResource(R.drawable.day);
        }else{
            background.setBackgroundResource(R.drawable.night);
        }
    }

    private void zeroOutCounters(){
        for(TextView t : voteCounters){
            t.setText("Votes 0");
        }
    }

  /*  public class BackgroundMusicService extends AsyncTask<Void , Void , Void>{
        @Override
        protected Void doInBackground(Void... params) {
            MediaPlayer player = MediaPlayer.create(GameNightActivity.this, R.raw.backgroundmusic);
            player.setLooping(true);
            player.setVolume(100 , 100);
            player.start();
            return null;
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        mp.stop();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mp.start();
    }
}