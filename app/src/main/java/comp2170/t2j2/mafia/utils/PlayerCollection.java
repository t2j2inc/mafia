package comp2170.t2j2.mafia.utils;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

import comp2170.t2j2.mafia.models.Player;

public class PlayerCollection {

    private static final String TAG = "Players";
    private static final String FILENAME = "players.json";
    private ArrayList<Player> players;
    private PlayerJsonSerializer serializer;
    private static  PlayerCollection playerCollection;
    private Context context;

    private PlayerCollection(Context context){
       this.context = context;
       serializer = new PlayerJsonSerializer(context , FILENAME);
       try {
           players = serializer.loadPlayers();
       }catch (Exception e){
           players = new ArrayList<>();
           Log.e(TAG , "Error loading players: " , e);
       }
    }

    public static PlayerCollection get(Context c){
        if(playerCollection == null){
            playerCollection = new PlayerCollection(c.getApplicationContext());
        }
        return playerCollection;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public Player getPlayer(UUID id){
        for(Player p : players){
            if(p.getId().equals(id))
                return p;
        }
        return null;
    }

    public boolean savePlayers(){
        try{
            serializer.savePlayers(players);
            Log.d(TAG , "player saved");
            return true;
        }catch(Exception e){
            Log.e(TAG , "Error saving plyers: " , e);
            return false;
        }
     }

    public void addPlayer(Player player){
        players.add(player);
    }

    public ArrayList<String> getPlayerNames(){
        ArrayList<String> names = new ArrayList<String>();
        for(int i = 0; i < players.size(); i++){
            names.add(players.get(i).getName());
        }
        return names;
    }

    public void deletePlayer(int position){
        players.remove(position);
    }
}
