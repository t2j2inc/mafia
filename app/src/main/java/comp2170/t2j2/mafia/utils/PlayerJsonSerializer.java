package comp2170.t2j2.mafia.utils;


import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import comp2170.t2j2.mafia.models.Player;

public class PlayerJsonSerializer {

    private Context context;
    private String fileName;

    public PlayerJsonSerializer(Context c , String f){
        context = c;
        fileName = f;
    }

    public void savePlayers(ArrayList<Player> players) throws JSONException, IOException{
        JSONArray array = new JSONArray();
        for(Player p: players)
            array.put(p.toJson());
        Writer writer = null;
        try{
            OutputStream out = context.openFileOutput(fileName , Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        }finally {
            if(writer != null)
                writer.close();
        }
    }


    public ArrayList<Player> loadPlayers() throws IOException , JSONException{
        ArrayList<Player> players = new ArrayList<>();
        BufferedReader reader = null;
        try{
            InputStream in = context.openFileInput(fileName);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                jsonString.append(line);
            }
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
            for(int i = 0; i < array.length(); i++){
                players.add(new Player(array.getJSONObject(i)));
            }
        }catch (FileNotFoundException e){
            //Happens when you have no registered players
        }
        finally {
            if(reader != null){
                reader.close();
            }
        }
        return  players;
    }
}

