package comp2170.t2j2.mafia.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomFont extends TextView{
        private Typeface typeface;

        public CustomFont(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public CustomFont(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public CustomFont(Context context) {
            super(context);
            init();
        }

        private void init() {
           typeface = Typeface.createFromAsset(getContext().getAssets(), "roboto.ttf");
           setTypeface(typeface);
        }
    }