package comp2170.t2j2.mafia.models;

public enum Role {
    MAFIA , SHERIFF , DOCTOR , VILLAGER, DEAD
}