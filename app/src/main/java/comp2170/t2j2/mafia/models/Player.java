package comp2170.t2j2.mafia.models;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class Player {

    private static final String JSON_ID = "id";
    private static final String JSON_NAME = "name";
    private UUID id;
    private String name;
    private Role role;
    private int votes;
    private Player visit;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    public int getVotes(){
        return votes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Player(String name) {
        id = UUID.randomUUID();
        this.name = name;
        role = null;
        votes = 0;
    }



    public Player(JSONObject json) throws JSONException{
        id = UUID.fromString(json.getString(JSON_ID));
        if(json.has(JSON_NAME)){
            name = json.getString(JSON_NAME);
        }
    }

    public void receiveVote(){
        votes++;
    }

    public void castVote(Player player){
        player.receiveVote();
        this.visit = player;
    }

    public void resetVote(Player player){
        votes = 0;
    }

    public String toString(){
        return this.id + " " + this.name + " " + this.role;
    }

    public void setVotes(int num) {
        this.votes = num;
    }

    public JSONObject toJson() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(JSON_ID , id.toString());
        json.put(JSON_NAME , name);
        return json;
    }

    public Player getVisit(){
        return visit;
    }

    public void incrementVotes(){
        votes++;
    }
}