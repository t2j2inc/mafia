package comp2170.t2j2.mafia.models;

import java.util.ArrayList;

import java.util.Random;

public class Game {

    private Phase phase;
    private int round;
    private ArrayList<Player> players;
    private int mafiaCount;
    private int villageCount;
    private Role winner;
    public Role turn;

    public Phase getPhase() {
        return phase;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public int getRound() {
        return round;
    }

    private void determineWinner(){
        if(mafiaCount >= villageCount)
            winner = Role.MAFIA;
        else if(mafiaCount == 0)
            winner = Role.VILLAGER;
        else
            winner = null;
    }

    private void setCounts(){
        mafiaCount = 0;
        villageCount = 0;
        for(int playerIndex = 0; playerIndex < players.size(); playerIndex++){
            if(players.get(playerIndex).getRole() == Role.MAFIA)
                mafiaCount++;
            else if(players.get(playerIndex).getRole() != Role.DEAD)
                villageCount++;
        }
    }

    public void removePlayer(Player player){
        for(Player p : players){
            if(p.equals(player)){
                p.setRole(Role.DEAD);
                break;
            }
        }
        setCounts();
    }

    public boolean isGameOver(){
        determineWinner();
        return winner != null;
    }

    public Game(ArrayList<Player> players) {
        phase = Phase.NIGHT;
        round = 1;
        this.players = players;
        int index = getRandomIndex();
        players.get(index).setRole(Role.MAFIA);
        index = findUnassignedPlayerIndex(players, index);
        players.get(index).setRole(Role.MAFIA);
        index = findUnassignedPlayerIndex(players , index);
        players.get(index).setRole(Role.DOCTOR);
        index = findUnassignedPlayerIndex(players , index);
        players.get(index).setRole(Role.SHERIFF);
        assignVillagers();
        setCounts();
        turn = Role.MAFIA;
    }

    private int findUnassignedPlayerIndex(ArrayList<Player> players, int index) {
        while(players.get(index).getRole() != null){
            index = getRandomIndex();
        }
        return index;
    }

    private int getRandomIndex(){
        Random randomIndex = new Random();
        int index = randomIndex.nextInt(players.size());
        return index;
    }

    private  void assignVillagers(){
        for(int index = 0; index < players.size(); index++){
            if(players.get(index).getRole() == null)
                players.get(index).setRole(Role.VILLAGER);
        }
    }

    public void changePhase(){
        if(phase == Phase.NIGHT )
            phase = phase.DAY;
        else{
            phase = Phase.NIGHT;
            round++;
        }
    }

    public Role getWinner() {
        return this.winner;
    }

    public void incrementVotes(int position){
        players.get(position).incrementVotes();
    }

    public Player mostVotesPlayer(){
        int mostVotes = 0;
        Player mostPlayer = null;
        for(Player p: players){
            if(p.getVotes() > mostVotes){
                mostPlayer = p;
                mostVotes = p.getVotes();
            }
        }
        return mostPlayer;
    }

    public void resetPlayerVotes(){
        for(Player p: players){
            p.setVotes(0);
        }
    }

    public void changeRound(){
        this.round++;
    }
}

